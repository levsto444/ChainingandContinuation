﻿using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using System.Linq;

public class ChainingandContinuation
{
    public static void Main()
    {
        Task<int[]> task1 = Task.Run(() =>
        {
            return RandomArray();
        });

        Console.WriteLine(string.Join(",", task1.Result));

        Task<int[]> task2 = task1.ContinueWith((x) =>
        {
            return MultipleArray(task1.Result);
        });

        Console.WriteLine(string.Join(",", task2.Result));

        Task<int[]> task3 = task2.ContinueWith((x) =>
        {
            return SortArray(task2.Result);
        });

        Console.WriteLine(string.Join(",", task3.Result));

        Task<double> task4 = task3.ContinueWith((x) =>
        {
            return Average(task3.Result);
        });

        Console.WriteLine(task4.Result);
    }

    public static int[] RandomArray()
    {
        int Min = 0;
        int Max = 50;

        int[] arr = new int[10];

        Random randNum = new();
        for (int i = 0; i < arr.Length; i++)
        {
            arr[i] = randNum.Next(Min, Max);
        }

        return arr;
    }

    public static int[] MultipleArray(int[] arr)
    {
        int Min = 0;
        int Max = 8;
        Random random = new();
        int multiplier = random.Next(Min, Max);

        for (int i = 0; i < arr.Length; ++i)
            arr[i] *= multiplier;

        return arr;
    }

    public static int[] SortArray(int[] arr)
    {
        return arr.OrderBy(x => x).ToArray();
    }

    public static double Average(int[] arr)
    {
        return Queryable.Average(arr.AsQueryable());
    }
}

